from setuptools import find_packages, setup

setup(
    name='demoapp',
    version='1.0.0',
    description='Trivial Webapp for Ansible presentation',
    license="MIT",
    author='Martin Obrist',
    author_email='dev@obrist.email',
    packages=find_packages(where='.', exclude=['tests', 'tasks']),
    python_requires='>=3.8, <4',
    include_package_data=True,
    setup_requires=['wheel'],
    install_requires=[
        "fastapi",
        "requests",
        "pydantic",
        "uvicorn"
    ],
    extras_require={
    }
)
