from enum import Enum


class ComponentEnum(Enum):
    FRONTEND = 1
    BACKEND = 2
