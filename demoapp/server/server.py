import uvicorn
import asyncio

from demoapp.server import ComponentEnum, DemoApplication


class DemoServer:

    def __init__(self, component: ComponentEnum):
        self.app = DemoApplication(component)

    async def serve(self):
        srvconfig = uvicorn.Config(
            self.app,
            host="0.0.0.0",
            port=5000,
            lifespan="off",
            access_log=False
        )
        server = uvicorn.Server(config=srvconfig)
        server.install_signal_handlers = lambda: None
        await server.serve()

    def run(self):
        """Use asyncio to start the async server applicaton"""
        asyncio.run(self.serve())
