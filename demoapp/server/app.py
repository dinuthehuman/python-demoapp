from pathlib import Path
from pydantic import BaseModel
import requests
from fastapi import FastAPI, APIRouter
from fastapi.responses import HTMLResponse, JSONResponse

from demoapp.server import ComponentEnum


def get_ip() -> str:
    req = requests.get("https://api.ipify.org")
    return req.text


class BackendResponse(BaseModel):
    msg: str
    ip: str

    @classmethod
    def default(cls):
        return cls(
            msg="Hello from Frankfurt!",
            ip=get_ip()
        )


class DemoApplication(FastAPI):
    """FastAPI app to run with uvicorn, contains all api endpoints as routes"""

    def __init__(self, component: ComponentEnum):
        super().__init__(
            title="Demo Application",
            version="1.0.0"
        )
        self.component = component
        self._add_routes()

    @staticmethod
    def get_backend_response() -> BackendResponse:
        with Path("/root/backendaddr.txt").open() as f:
            backend_ip = f.read().strip()
        req = requests.get(f"http://{backend_ip}:5000")
        resp = BackendResponse.model_validate_json(req.text)
        return resp

    @staticmethod
    def get_frontend_router() -> APIRouter:

        router = APIRouter()

        @router.get("/", response_class=HTMLResponse)
        async def get_root():
            backend_resp = DemoApplication.get_backend_response()
            return f"""
            <html>
                <head>
                    <title>Demo Frontend</title>
                </head>
                <body>
                    <h3>Frontend Server</h3>
                    <p>Hello from San Francisco!</p>
                    <p>My IP address: {get_ip()}</p>
                    <h3>Response from Backend</h3>
                    <p>Message: {backend_resp.msg}</p>
                    <p>Backend IP: {backend_resp.ip}
                </body>
            </html>
            """

        return router

    @staticmethod
    def get_backend_router() -> APIRouter:
        router = APIRouter()

        @router.get("/", response_class=JSONResponse)
        async def get_root():
            return BackendResponse.default()

        return router

    def _add_routes(self):
        """Add all endpoint router to the application"""
        if self.component == ComponentEnum.FRONTEND:
            router = DemoApplication.get_frontend_router()
        else:
            router = DemoApplication.get_backend_router()
        self.include_router(router=router)
