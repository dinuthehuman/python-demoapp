from demoapp.server import ComponentEnum, DemoServer


if __name__ == "__main__":
    DemoServer(ComponentEnum.FRONTEND).run()
